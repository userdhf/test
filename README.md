# gojs流程图测试


# 安装 和 启动

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## 说明
### 仅做测试。未全局注入jq和gojs，仅限组件内调试使用。组件路由/go。
### 参考网址 https://gojs.net/latest/samples/productionProcess.html



